/**
 * Board module
 * Does all actions on the board
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#ifndef CHESS_BOARD_H
#define CHESS_BOARD_H

#include "selection.h"
#include "figure.h"
#include <ostream>
#include <vector>
#include "notation.h"

class Figure;
class King;
class Notation;

/**
 * Board class
 * Handles piece moves and holds board information
 */
class Board {
public:
    static const short BOARD_SIZE = 8;
private:
    Figure *board[BOARD_SIZE][BOARD_SIZE];
    bool white_move;
    int selected_col;
    int selected_row;
    King *white_king;
    King *black_king;
    std::vector<Selection *> selection;
    Selection *check_field;
    Selection *moved_from;
    Selection *moved_to;
    Selection *pinned;
    Notation *notation;
public:
    /**
     * Constructor
     * @note calls reset()
     */
    Board();

    /**
     * Resets the board to default chess starting position (sets up pieces)
     */
    void reset();

    /**
     * Selects field making move or selecting piece
     * @param col Column of the piece or field
     * @param row Row of the piece or field
     */
    void selected(int col, int row);

    /**
     * Selects piece on board
     * @param col Column of the piece
     * @param row Row of the piece
     * @return if selection was successful (correct color and field if not empty)
     */
    bool select_piece(int col, int row);

    /**
     * Moves selected piece to passed in position
     * @param col Column to move to
     * @param row Row to move to
     * @return if moving was successful
     */
    bool move(int col, int row);

    /**
     * Returns figure at passed in position
     * @param col Column
     * @param row Row
     * @return figure at position of [col][row] or nullptr if no piece is at that position
     */
    Figure *get_figure_at(int col, int row);

    /**
     * @return white king
     */
    King *getWhiteKing();

    /**
     * @return black king
     */
    King *getBlackKing();

    /**
     * Places figure on to board at the passed in position
     * @param col Column
     * @param row Row
     * @param f Figure to be placed
     */
    void set_figure_at(int col, int row, Figure *f);

    /**
     * Checks if the passed in position is attacked
     * (this method is used for kings, thus the name)
     * @param col Column
     * @param row Row
     * @param white if the piece is white
     * @return if the passed in position is threatened
     */
    bool is_check(int col, int row, bool white);

    /**
     * Returns all figures that are on the board
     * @return Vector of figure pointers
     */
    std::vector<Figure *> get_figures();

    /**
     * Returns all selections that are now present
     * @return Vector of selection pointers
     */
    std::vector<Selection *> get_selections();

    /**
     * Moves a piece from one field to another and doesn't count as a user move
     * @warning This function doesn't do any checks, so it should be used carefully
     * @param col_from
     * @param row_from
     * @param col_to
     * @param row_to
     */
    void select_piece_and_move(int col_from, int row_from, int col_to, int row_to);

    /**
     * Sets notation object (should be called only once after parsing is done)
     * @param notation notation object that was called with this object
     */
    void set_notation(Notation *notation);

#ifdef DEBUG
    /**
     * Printing operator.
     * Displays current board state in ASCII to stdout
     * @param os ostream
     * @return ostream
     */
    void print(std::ostream& os);
#endif

};

#endif //CHESS_BOARD_H
