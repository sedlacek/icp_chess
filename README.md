#ICP projekt

## Souřadnice
Pokud uživatel klikne tak očekávané zaslané souřednice jsou v Cčkové podobě, tedy políčko `a:1` (bílá věž) ma souřadnice `0:7`, `a:8` = `0:0`, `e:5` = `4:3`...

```
[0:0]+--+--+---
     |__|__|__
     |__|__|__
     |  |  |    ...
                [7:7]
```

## Jak funguje pohyb/kontroly/indikátory
Protože uživatel jenom kliká na místa na obrazovce, proto interakce s figurkama je ve stylu, že kliknutí invokuje (by mělo invokovat) metodu `void selected(int col, int row);`, která pokud se kliklo na figurku a není žádná vybraná, tak tuto figurku zvýrazní (podbarví se) a zobrazí se políčka kam může figurka jít/koho vzít (je to napodobenina toho jak toto funguje na liches.org) - viz obrazek.

![chess](https://i.imgur.com/oVfdYp6.png)

_Obr 1: Tečky jsou políčka kde může dáma jít, políčka s okraji jsou figurky které může vzít_

Tyto políčka jsou realizovaná třídou `Selection` a všechny jsou generováný poté co se vybere figurka a pomocí nich se i kontroluje to když figurku se pokusí hráč pohnout.

Pohutí tedy probíhá uplně stejně - invokuje se metoda `selected` a pokud se na poslané políčko figurka může pohnout, tak se tak stane, jinak se pouze zruší její výběr (opět stejně jako na lichess)

Poté jde na obrázku 1 vidět, že ještě je na poli zvýrazněno co byl poslední tah (z kama kam) opět ve stylu lichess

Také je zde políčko zvýraznění pro šach (červené na obrázku), pokud je král v šachu a hráč pohne figurkou tak, že to nezabrání matu, tak mu to hra nepovolí (toto je v mojí Java verzi také zvýrazněné a můžeme to mít i tady) - šach mat ale nemám přidaný že se rozeznává, protože je to náročnější a myslím, že to není potřeba

![check](https://i.imgur.com/WjbcfN1.png)

_Obr 2: Král je v šachu, tedy svítí červeně_

## Rozhraní pro komunikaci GUI s hrou
GUI by po načtení hry mělo vytvořit novou instanci Game a přes ni komunikovat, jsou zde pro to metody:

* Konstruktor `Game(char *path);` - kde path je cesta k souboru co chce uživatel otevřít. Pokud by vyhovovalo víc tam mít `FILE *` není to problém.
* `void save(char *path);` - Uloží momentálně rozehranou hru do souboru co se pošle. Bude generovat vyjímku pokud se zapis nepovede (tu ještě nemám specifikovanou, ale bude to objekt co obsahuje zprávu jaka chyba nastala)
* `void reset();` - Restaruje hru na tak jak byla po načtení
* `void next_move();` - Nastane po zmáčknuti tlačítka aby nastal další move, popř bude invokované timerem pokud uživatel spustí autoplay
* `void prev_move();` - To same jako next, ale nazpět
* `void set_move(int number);` - Nastaví v kolikátem tahu se má hra nacházet - 0 je začatek (žádný tah)
* `void selected(int col, int row);` - viz _Jak funguje pohyb/kontroly/indikátory_

Všechny metody které nastavuji tahy, si kontrolují jestli jde daná akce udělat, takže není potřeba toto kontrolovat v GUI, buď se akce (např další move) stane nebo ne, nic by nemělo spadnout.

## Co je potřeba mít
Vykreslovací metody pro figurky - nějaká virtualní metoda ve `figure`, kde se vykreslí obrázek který si objekt načte popř může tato metoda volat nějakou v GUI

Vykreslovací metody pro `Selected` (zvýrazněné políčka) - Nejlepší by bylo mít nějakou metodu kde bych mohl políčko poslat se souřadnicemi a ono by se vykreslilo popř. by se tam poslalo pole (to by ale bylo modifikované při každém vykreslování, protože někdy se políčka překrývají a pak je potřeba vykreslit jen to nejdůležitější - např. poslední tah je občas překrytý šachem).

