/**
 * Notation module
 * Handles all operations over notation
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#ifndef CHESS_NOTATION_H
#define CHESS_NOTATION_H

#include "board.h"
#include "defines.h"
#include "figure.h"
#include <vector>
#include <array>

class Board;
class Figure;
enum class figure_t;

/**
 * Notation class
 */
class Notation {
private:
    Board *board;
    using ParsedMoves = std::array<std::string, 3>;
    /**
     * Private class used to save moves
     */
    class Move {
    private:
        Coordinates move_from;
        Coordinates move_to;
        Figure *moved;
        Figure *taken;
    public:
        Move(Coordinates move_from, Coordinates move_to, Figure *moved, Figure *taken): move_from(move_from), move_to(move_to), moved(moved), taken(taken) {}
        Coordinates get_from(){return move_from;}
        Coordinates get_to(){return move_to;}
        Figure *get_taken(){return taken;}
        Figure *get_moved(){ return moved;}
    };

    std::vector<Move> original_moves;
    std::vector<Move> moves;
    std::vector<ParsedMoves> parsed;
    std::string path;
    int current_move;
    bool user_mode;

    void make_move(std::string move, Board *board, bool whites_move, int line);
    static int convert_coordinates(std::string pos, Coordinates &coords);
    static std::string convert_coordinates(int col, int row);
    std::vector<Coordinates> get_figure(figure_t ftype, bool white, int to_col, int to_row);
public:

    /**
     * Constructor
     * @param path Path to the file to be parsed, if the path is an empty string, then the created notation will
     *  be empty
     * @param board Board on which will be this notation represented and parsed
     */
    Notation(std::string path, Board *board);

    /**
     * Parses file supplied in constructor.
     * Does only syntactical analysis
     * @note HAS TO BE called before check_moves() is called
     * @throw ChessException if there's an error in parsing of the file or opening it
     */
    void parse();

    /**
     * Checks parsed notation file semantically
     * @throw ChessException if there's an error in parsing the notation
     */
    void check_moves();

    /**
     * Saves notation into passed in file
     * @param path Path to the file to which should be the notation saved
     */
    void save(std::string path);

    /**
     * Makes next move if possible
     */
    void next_move();

    /**
     * Undo a move if possible
     */
    void prev_move();

    /**
     * Set the game to position (move) passed in
     * Numbering starts from 0 (1st move is number 0)
     * @param number position to move to
     */
    void set_move(int number);

    /**
     * Resets the game to starting position
     */
    void reset();

    /**
     * Adds new move to the current notation.
     * All the moves after current position will be removed.
     * @param f Figure which was moved
     * @param taken Figure which was taken by this move or nullptr if no figure was taken
     * @param col_from Column from which the figure moved
     * @param row_from Row from which the figure moved
     * @note HAS TO BE called after the move was done
     */
    void user_move(Figure *f, Figure *taken, int col_from, int row_from);

    /**
     * Formats the notation into array of strings
     * @return The formatted notation
     */
    std::vector<NotationRow> format();

    /**
     * Returns current active move (0 = no move played yet, 2 = blacks 1st move)
     * @return current move
     */
    int get_current_move();
};


#endif //CHESS_NOTATION_H
