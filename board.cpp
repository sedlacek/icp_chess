/**
 * Board module
 * Does all actions on the board
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#include "board.h"

Board::Board() {
    notation = nullptr;
    this->reset();
}

void Board::reset() {
    this->white_move = true;
    this->selection.clear();
    this->check_field = nullptr;
    this->moved_from = nullptr;
    this->moved_to = nullptr;
    this->pinned = nullptr;
    for(int i = 0; i < BOARD_SIZE; i++) {
        for(int a = 0; a < BOARD_SIZE; a++) {
            board[i][a] = nullptr;
        }
    }

    // Pawns
    for(int i = 0; i < BOARD_SIZE; i++) {
        board[i][1] = new Pawn(i, 1, false, this);
    }
    for(int i = 0; i < BOARD_SIZE; i++) {
        board[i][6] = new Pawn(i, 6, true, this);
    }
    // Rooks
    board[0][0] = new Rook(0, 0, false, this);
    board[7][0] = new Rook(7, 0, false, this);
    board[0][7] = new Rook(0, 7, true, this);
    board[7][7] = new Rook(7, 7, true, this);
    // Knights
    board[1][0] = new Knight(1, 0, false, this);
    board[6][0] = new Knight(6, 0, false, this);
    board[1][7] = new Knight(1, 7, true, this);
    board[6][7] = new Knight(6, 7, true, this);
    // Bishops
    board[2][0] = new Bishop(2, 0, false, this);
    board[5][0] = new Bishop(5, 0, false, this);
    board[2][7] = new Bishop(2, 7, true, this);
    board[5][7] = new Bishop(5, 7, true, this);
    // Queens
    board[3][0] = new Queen(3, 0, false, this);
    board[3][7] = new Queen(3, 7, true, this);
    // Kings
    this->black_king = new King(4, 0, false, this);
    board[4][0] = black_king;
    this->white_king = new King(4, 7, true, this);
    board[4][7] = white_king;
}

void Board::selected(int col, int row) {
    if(this->selection.empty()){ // No piece is selected
        this->select_piece(col, row);
    }
    else{ // Piece is selected and will be moved or selection canceled
        this->move(col, row);
        this->selection.clear();
    }
}

bool Board::select_piece(int col, int row) {
    if(board[col][row] != nullptr && board[col][row]->isWhite() == this->white_move){
        this->selected_col = col;
        this->selected_row = row;
        board[col][row]->addSelects(selection);
        return true;
    }
    return false;
}

bool Board::move(int col, int row) {
    // Cycle through all possible fields and if player clicked one of those - move there
    for(Selection *s: this->selection){
        if(s->getType() != selection_t::SELECTED && s->getCol() == col && s->getRow() == row){
            Figure *taken = board[col][row];
            board[col][row] = board[selected_col][selected_row];
            board[col][row]->setColRow(col, row);
            board[selected_col][selected_row] = nullptr;
            check_field = nullptr;
            pinned = nullptr;
            if(is_check(black_king->getCol(), black_king->getRow(), black_king->isWhite())){
                // Black in check - allow only moves that doesn't lead to mate if it's black's turn
                if(!white_move){
                    board[selected_col][selected_row] = board[col][row];
                    board[col][row] = taken;
                    board[selected_col][selected_row]->setColRow(selected_col, selected_row);
                    this->pinned = new Selection(selected_col, selected_row, selection_t::PINNED);
                    this->check_field = new Selection(black_king->getCol(), black_king->getRow(), selection_t::CHECK);
                    return false;
                }
                this->check_field = new Selection(black_king->getCol(), black_king->getRow(), selection_t::CHECK);
            }
            else if(is_check(white_king->getCol(), white_king->getRow(), white_king->isWhite())){
                // White in check - allow only moves that doesn't lead to mate if it's white's turn
                if(white_move){
                    board[selected_col][selected_row] = board[col][row];
                    board[col][row] = taken;
                    board[selected_col][selected_row]->setColRow(selected_col, selected_row);
                    this->pinned = new Selection(selected_col, selected_row, selection_t::PINNED);
                    this->check_field = new Selection(white_king->getCol(), white_king->getRow(), selection_t::CHECK);
                    return false;
                }
                this->check_field = new Selection(white_king->getCol(), white_king->getRow(), selection_t::CHECK);
            }

            white_move = !white_move;
            if(notation != nullptr)
                this->notation->user_move(board[col][row], taken, selected_col, selected_row);
            this->selection.clear();
            this->moved_from = new Selection(col, row, selection_t::LAST_MOVE);
            this->moved_to = new Selection(selected_col, selected_row, selection_t::LAST_MOVE);
            return true;
        }
    }
    return false;
}

void Board::select_piece_and_move(int col_from, int row_from, int col_to, int row_to) {
    this->selection.clear();
    board[col_to][row_to] = board[col_from][row_from];
    board[col_to][row_to]->setColRow(col_to, row_to);
    this->moved_from = new Selection(col_from, row_from, selection_t::LAST_MOVE);
    this->moved_to = new Selection(col_to, row_to, selection_t::LAST_MOVE);
    white_move = !white_move;
    this->check_field = nullptr;
    this->pinned = nullptr;
    board[col_from][row_from] = nullptr;
    if(is_check(black_king->getCol(), black_king->getRow(), black_king->isWhite())){
        this->check_field = new Selection(black_king->getCol(), black_king->getRow(), selection_t::CHECK);
    }
    else if(is_check(white_king->getCol(), white_king->getRow(), white_king->isWhite())){
        this->check_field = new Selection(white_king->getCol(), white_king->getRow(), selection_t::CHECK);
    }
}

bool Board::is_check(int col, int row, bool white) {
    for(int x = 0; x < BOARD_SIZE; x++) {
        for(int y = 0; y < BOARD_SIZE; y++) {
            Figure *f = board[x][y];
            if(f == nullptr || f->isWhite() == white || f->getType() == figure_t::KING)
                continue;
            std::vector<Selection *> sels;
            f->addSelects(sels);
            for(Selection *s: sels) {
                if(s->getCol() == col && s->getRow() == row) {
                    return true;
                }
            }
        }
    }
    return false;
}

Figure *Board::get_figure_at(int col, int row) {
    return board[col][row];
}

void Board::set_figure_at(int col, int row, Figure *f) {
    board[col][row] = f;
}

King *Board::getBlackKing() {
    return this->black_king;
}

King *Board::getWhiteKing() {
    return this->white_king;
}

std::vector<Figure *> Board::get_figures(){
    std::vector<Figure *> figs;
    for(int x = 0; x < BOARD_SIZE; x++){
        for(int y = 0; y < BOARD_SIZE; y++){
            if(board[x][y] != nullptr){
                figs.push_back(board[x][y]);
            }
        }
    }
    return figs;
}

std::vector<Selection *> Board::get_selections(){
    std::vector<Selection *> ret_vec(this->selection);
    if(this->check_field != nullptr)
        ret_vec.push_back(this->check_field);

    bool from_is_in = false;
    bool to_is_in = false;
    bool pinned_in = false;
    for (Selection *s: this->selection) {
        if(this->moved_from != nullptr && s->getCol() == moved_from->getCol() && s->getRow() == moved_from->getRow()){
            from_is_in = true;
        }
        if(this->moved_to != nullptr && s->getCol() == moved_to->getCol() && s->getRow() == moved_to->getRow()){
            to_is_in = true;
        }
        if(this->pinned != nullptr && s->getCol() == pinned->getCol() && s->getRow() == pinned->getRow()){
            pinned_in = true;
        }
    }
    if(!from_is_in && this->moved_to != nullptr)
        ret_vec.push_back(this->moved_from);
    if(!to_is_in && this->moved_to != nullptr)
        ret_vec.push_back(this->moved_to);
    if(!pinned_in && this->pinned != nullptr)
        ret_vec.push_back(this->pinned);

    return ret_vec;
}

void Board::set_notation(Notation *notation) {
    this->notation = notation;
}

#ifdef DEBUG
void Board::print(std::ostream& os){
    std::string lines = "";
    os << "\033[1;34m   0   1   2   3   4   5   6   7\n\033[0m";
    for(int y = 0; y < BOARD_SIZE; y++){
        os << "\033[1;34m" << 8-y << "\033[0m" << "|";
        for(int x = 0; x < BOARD_SIZE; x++){
            std::string back = "\033[1;49m";
            for(Selection *s: get_selections()){
                if(x == s->getCol() && y == s->getRow()){
                    back = s->getBGColor();
                }
            }
            os << back;
            if(board[x][y] == nullptr){
                os << "   ";
            }
            else if(board[x][y] != nullptr && board[x][y]->getType() == figure_t::PAWN){
                if(board[x][y]->isWhite())
                    os << " p ";
                else
                    os << " \033[1;31mp \033[0m";
            }
            else {
                if(board[x][y]->isWhite())
                    os << " " << board[x][y]->getIdentificator() << " ";
                else
                    os << " " << "\033[1;31m"<< board[x][y]->getIdentificator() <<" \033[0m";
            }
            os << "\033[0m|";
        }
        os << "\033[1;34m" << y << "\033[0m";
        os << "\n";
    }
    os << "\033[1;34m   A   B   C   D   E   F   G   H\n\033[0m";
}
#endif