/**
 * Selection module
 * Module for working with highlights on the board
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#ifndef CHESS_SELECTION_H
#define CHESS_SELECTION_H

#include <string>
#include "defines.h"

/**
 * Type of field
 */
enum class selection_t{
    SELECTED,   // Piece is selected
    MOVABLE,    // Piece can move to this field
    THREATENED, // Piece can take figure on this field
    LAST_MOVE,  // Last move
    CHECK,      // King is in check
    PINNED,     // (Absolute pin) Piece cannot move, if it would it would unprotect king (allowing mate)
};

/**
 * Selection class
 * Holds coordinates and type of selection
 */
class Selection{
private:
    int col;
    int row;
    selection_t type;
public:
    /**
     * Constructor
     * @param col Column of the field
     * @param row Row of the field
     * @param type Type of selection @see selection_t
     */
    Selection(int col, int row, selection_t type);

    /**
     * @return selections column
     */
    int getCol();

    /**
     * @return selections row
     */
    int getRow();

    /**
     * @return selections type
     */
    selection_t getType();

#ifdef DEBUG
    std::string getBGColor();
#endif
};

#endif //CHESS_SELECTION_H
