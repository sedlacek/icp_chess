/**
 * Defines module
 * Holds declarations for shared data types
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#ifndef ICP_CHESS_DEFINES_H
#define ICP_CHESS_DEFINES_H

#include <tuple>
#include <string>
#include <array>

#define DEBUG

using Coordinates = std::tuple<int, int>;  //< Tuple of coordinates
using NotationRow = std::array<std::string, 3>;  //< Array of 3 strings

/**
 * Exception class
 * (all methods are inline)
 */
class ChessException {
private:
    std::string message;
public:

    /**
     * Constructor of the exception
     * @param message Error message explaining the error
     */
    ChessException(std::string message): message{message} {}

    /**
     * @return getter for the error message
     */
    std::string get_message() { return this->message; }
};

#endif //ICP_CHESS_DEFINES_H
