/**
 * Notation module
 * Handles all operations over notation
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#include "notation.h"
#include "defines.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <regex>
#include <cctype>
#include "figure.h"
#include <fstream>

Notation::Notation(std::string path, Board *board) {
    this->board = board;
    this->path = path;
}

void Notation::parse() {
    if(this->path.empty()){
        return;
    }
    using namespace std;
    ifstream file(this->path);
    string line;
    int line_numb = 1;
    regex pattern_correct("[KDVSJp]?[a-h]?[1-8]?[x]?[a-h][1-8][+#]?");
    regex pattern_incorrect(".*[ijklmnoqrstuvwyzABCEFGHILMNOPQRTUWXYZ].*");
    if (file.is_open()) {
        bool was_last = false;
        while (getline(file, line)) {
            // Make istringstream and ten parse it into vector of string
            istringstream str_stream(line);
            vector <string> tokens(istream_iterator<string>(str_stream), istream_iterator < string > {});
            if (was_last)
                throw new ChessException("Missing black's move at line " + to_string(line_numb));
            if (tokens.size() < 2 || tokens.size() > 3)
                throw new ChessException("Incorrect amount of columns at line " + to_string(line_numb));
            if (to_string(line_numb) + "." != tokens[0])
                throw new ChessException("Incorrect move number at line " + to_string(line_numb));
            if (!regex_match(tokens[1], pattern_correct) || regex_match(tokens[1], pattern_incorrect))
                throw new ChessException("White's move has incorrect format");
            if (tokens.size() == 3) {
                if (!regex_match(tokens[2], pattern_correct) || regex_match(tokens[2], pattern_incorrect))
                    throw new ChessException("Black's move has incorrect format");
            } else { // only white's move (thus this should be final line)
                was_last = true;
            }

            this->parsed.push_back({tokens[0], tokens[1], (tokens.size() == 3) ? tokens[2] : string()});
            line_numb++;
        }

        file.close();
    } else {
        throw new ChessException("Could not open file '" + this->path + "'");
    }
}

std::vector<Coordinates> Notation::get_figure(figure_t ftype, bool white, int to_col, int to_row) {
    std::vector<Selection *> sels;
    std::vector<Coordinates> coords;
    Figure *f;

    // For pawns check just 3 veritcal strips
    for(int x = (ftype == figure_t::PAWN ? to_col-1 : 0); x <= (ftype == figure_t::PAWN ? to_col+1 : 7); x++){
        if(x < 0 || x > 7)
            continue;
        for(int y = 0; y < 8; y++){
            if((f = board->get_figure_at(x, y)) == nullptr || f->isWhite() != white || f->getType() != ftype){
                continue;
            }

            f->addSelects(sels);
            for(Selection *s: sels){
                if(s->getCol() == to_col && s->getRow() == to_row){
                    coords.push_back(Coordinates{x, y});
                }
            }
            sels.clear();
        }
    }

    return coords;
}

void Notation::make_move(std::string move, Board *board, bool whites_move, int line){
    using namespace std;
    Coordinates coord, from;

    int index = convert_coordinates(move, coord);
    figure_t c = figure_t::PAWN;
    bool was_taken = false;

    if(index > 0){
        if(move[index-1] == 'x'){ // taking piece
            index--;
            was_taken = true;
        }

        if(string("KDVSJp").find(move[index-1]) != string::npos){
            switch(move[index-1]){
                case 'K':
                    c = figure_t::KING;
                    break;
                case 'D':
                    c = figure_t::QUEEN;
                    break;
                case 'V':
                    c = figure_t::ROOK;
                    break;
                case 'S':
                    c = figure_t::BISHOP;
                    break;
                case 'J':
                    c = figure_t::KNIGHT;
                    break;
                case 'p':
                    c = figure_t::PAWN;
                    break;
            }

            vector<Coordinates> all_coords = get_figure(c, whites_move, get<0>(coord), get<1>(coord));
            if(all_coords.size() > 1)
                throw new ChessException("Move has to be further more specified at line "+to_string(line));
            if(all_coords.empty())
                throw new ChessException("Incorrect piece specification at line "+to_string(line));
            from = all_coords[0];
        }else { // Full specification
            if(!was_taken || index > 2 || (index == 2 && string("KDVSJp").find(move[0]) == string::npos)){
                int from_index = convert_coordinates(move.substr(0, index), from);
                if(get<0>(from) < 0 || get<1>(from) > 7)
                    throw new ChessException("Incorrect position at line "+to_string(line));

                if(from_index > 0 && string("KDVSJp").find(move[from_index-1]) != string::npos){
                    switch(move[from_index-1]){
                        case 'K':
                            c = figure_t::KING;
                            break;
                        case 'D':
                            c = figure_t::QUEEN;
                            break;
                        case 'V':
                            c = figure_t::ROOK;
                            break;
                        case 'S':
                            c = figure_t::BISHOP;
                            break;
                        case 'J':
                            c = figure_t::KNIGHT;
                            break;
                        case 'p':
                            c = figure_t::PAWN;
                            break;
                    }
                }
                if(board->get_figure_at(get<0>(from), get<1>(from))->getType() != c){
                    throw new ChessException("Incorrect piece specification at line "+to_string(line));
                }
            } else { // Just column or row specified
                if(move[0] > '1' && move[0] < '9') { // Row
                    int row = 8 - (move[0] - '0');
                    vector<Coordinates> all_c = get_figure(c, whites_move, get<0>(coord), get<1>(coord));
                    bool found = false;
                    for(Coordinates rcord: all_c){
                        if(get<1>(rcord) == row){
                            from = rcord;
                            if(found)
                                throw new ChessException("Move has to be further more specified at line "+to_string(line));
                            found = true;
                        }
                    }
                } else if(move[0] >= 'a' && move[0] <= 'h') { // Column
                    int col = move[0] - 'a';
                    vector<Coordinates> all_c = get_figure(c, whites_move, get<0>(coord), get<1>(coord));
                    bool found = false;
                    for(Coordinates rcord: all_c){
                        if(get<0>(rcord) == col){
                            from = rcord;
                            if(found)
                                throw new ChessException("Move has to be further more specified at line "+to_string(line));
                            found = true;
                        }
                    }
                } else {
                    throw new ChessException("Incorrect move format at line "+to_string(line));
                }
            }
        }
    }else{ // Pawn move
        vector<Coordinates> all_c = get_figure(c, whites_move, get<0>(coord), get<1>(coord));
        if(all_c.empty()){
            throw new ChessException("Piece cannot move to specified position at line "+to_string(line));
        }
        from = all_c[0];
    }

    this->moves.push_back(Move(from, coord, board->get_figure_at(get<0>(from), get<1>(from)), board->get_figure_at(get<0>(coord), get<1>(coord))));

    if(!board->select_piece(get<0>(from), get<1>(from))){
        throw new ChessException("Piece cannot move to specified position at line "+to_string(line));
    }
    if(!board->move(get<0>(coord), get<1>(coord))){
        throw new ChessException("Piece cannot move to specified position at line "+to_string(line));
    }
}

void Notation::check_moves() {
    if(this->path.empty())
        return;
    int line = 0;
    for(ParsedMoves p: this->parsed){
        line++;
        bool whites_move = true;
        for(unsigned short i = 1; i < p.size(); i++, whites_move = !whites_move){
            if(p[i].find_first_not_of(' ') != std::string::npos) {
                make_move(p[i], board, whites_move, line);
            }
        }
    }

    this->original_moves = this->moves;
}

std::string Notation::convert_coordinates(int col, int row) {
    char col_c = 'a'+col;
    return col_c+std::to_string(8-row);
}

int Notation::convert_coordinates(std::string pos, Coordinates &coords) {
    int row = 0;
    bool done = false;
    int a = 1;
    while(!done){
        if(std::isdigit(pos[pos.size()-a])){
            row = pos[pos.size()-a] - '0';
            done = true;
        }
        a++;
    }
    int col = pos[pos.size()-a]-'a';
    Coordinates c(col, 8-row);
    coords = c;
    return pos.size()-a;
}

void Notation::next_move() {
    if((unsigned int)current_move >= moves.size())
        return;
    Coordinates c1 = moves[current_move].get_from();
    Coordinates c2 = moves[current_move].get_to();
    board->select_piece_and_move(std::get<0>(c1), std::get<1>(c1), std::get<0>(c2), std::get<1>(c2));
    current_move++;
}

void Notation::prev_move() {
    if(current_move > 0){
        current_move--;
        Coordinates c1 = moves[current_move].get_from();
        Coordinates c2 = moves[current_move].get_to();
        board->select_piece_and_move(std::get<0>(c2), std::get<1>(c2), std::get<0>(c1), std::get<1>(c1));
        board->set_figure_at(std::get<0>(c2), std::get<1>(c2), moves[current_move].get_taken());
    }
}

void Notation::set_move(int number) {
    if(number < 0 || (unsigned int)number >= moves.size() || number == current_move-1)
        return;
    if(number < current_move) { // prev move
        for(int i = current_move-1; i > number; i--)
            prev_move();
    }
    else { // next move
        for(int i = current_move; i <= number; i++)
            next_move();
    }
}

void Notation::user_move(Figure *f, Figure *taken, int col_from, int row_from) {
    for(int i = this->moves.size()-1; i >= current_move; i--){
        moves.erase(moves.begin() + i);
    }
    this->moves.push_back(Move({col_from, row_from}, {f->getCol(), f->getRow()}, f, taken));
    current_move++;
}

std::vector<NotationRow> Notation::format() {
    std::vector<NotationRow> formatted;
    int move_numb = 1;
    for(unsigned int i = 0; i < moves.size(); i+=2, move_numb++){
        Coordinates c1 = moves[i].get_from();
        Coordinates c2 = moves[i].get_to();
        std::string repr = moves[i].get_moved()->getIdentificator() + convert_coordinates(std::get<0>(c1), std::get<1>(c1));
        if(moves[i].get_taken() != nullptr)
            repr += "x";
        repr += convert_coordinates(std::get<0>(c2), std::get<1>(c2));

        std::string repr2 = "";
        if(i+1 < moves.size()) {
            c1 = moves[i + 1].get_from();
            c2 = moves[i + 1].get_to();
            repr2 = moves[i + 1].get_moved()->getIdentificator() + convert_coordinates(std::get<0>(c1), std::get<1>(c1));
            if (moves[i+1].get_taken() != nullptr)
                repr2 += "x";
            repr2 += convert_coordinates(std::get<0>(c2), std::get <1>(c2));
        }

        NotationRow arr = {std::to_string(move_numb)+".", repr, repr2};
        formatted.push_back(arr);
    }
    return formatted;
}

void Notation::save(std::string path) {
    std::ofstream file;
    file.open(path);
    if(file.is_open()) {
        for (auto g: format()) {
            for (auto a: g) {
                file << a << " ";
            }
            file << std::endl;
        }
        file.close();
    }

}

void Notation::reset() {
    this->current_move = 0;
    this->moves = this->original_moves;
    this->user_mode = false;
}

int Notation::get_current_move() {
    return this->current_move;
}