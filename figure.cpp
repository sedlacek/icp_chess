/**
 * Figure module
 * Holds classes for all figures on the board
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#include "figure.h"

Figure::Figure(int col, int row, bool white, Board *board, figure_t type, std::string &&identificator) {
    this->col = col;
    this->row = row;
    this->white = white;
    this->board = board;
    this->identificator = identificator;
    this->type = type;
}

// Figure getters and setters
int Figure::getCol() {
    return this->col;
}

int Figure::getRow() {
    return this->row;
}

bool Figure::isWhite() {
    return this->white;
}

std::string Figure::getIdentificator() {
    return this->identificator;
}

void Figure::setColRow(int col, int row) {
    this->col = col;
    this->row = row;
}

figure_t Figure::getType(){
    return this->type;
}

// Pawn
void Pawn::addSelects(std::vector<Selection *> &selects) {
    selects.push_back(new Selection(this->col, this->row, selection_t::SELECTED));
    if((white && row == 0) || (!white && row == 7))
        return;
    if(board->get_figure_at(col, row + (white ? -1 : +1)) == nullptr){
        selects.push_back(new Selection(this->col, this->row + (white ? -1 : +1), selection_t::MOVABLE));
    }
    Figure *f;
    if(col > 0 && ((f = board->get_figure_at(col-1, row + (white ? -1 : +1))) != nullptr) && (f->isWhite() != white)) {
        selects.push_back(new Selection(col-1, row + (white ? -1 : +1), selection_t::THREATENED));
    }
    if(col < 7 && ((f = board->get_figure_at(col+1, row + (white ? -1 : +1))) != nullptr) && (f->isWhite() != white)) {
        selects.push_back(new Selection(col+1, row + (white ? -1 : +1), selection_t::THREATENED));
    }

    // First move (can move 2 squares)
    if((white && row == 6) || (!white && row == 1)) {
        if(board->get_figure_at(col, row + (white ? -2 : +2)) == nullptr) {
            selects.push_back(new Selection(col, row + (white ? -2 : +2), selection_t::MOVABLE));
        }
    }
}

// Knight
void Knight::addSelects(std::vector<Selection *> &selects) {
    selects.push_back(new Selection(this->col, this->row, selection_t::SELECTED));
    int movesX[] = {col-2, col-2, col-1, col-1, col+1, col+1, col+2, col+2};
    int movesY[] = {row-1, row+1, row-2, row+2, row-2, row+2, row+1, row-1};
    for(int i = 0; i < 8; i++) {
        int setCol = movesX[i];
        int setRow = movesY[i];
        if(setCol >= 0 && setRow <= 7 && setCol <= 7 && setRow >= 0) {
            if(board->get_figure_at(setCol, setRow) == nullptr)
                selects.push_back(new Selection(setCol, setRow, selection_t::MOVABLE));
            else if(board->get_figure_at(setCol, setRow)->isWhite() != white)
                selects.push_back(new Selection(setCol, setRow, selection_t::THREATENED));
        }
    }
}

// Bishop
void Bishop::addSelects(std::vector<Selection *> &selects) {
    selects.push_back(new Selection(this->col, this->row, selection_t::SELECTED));
    Figure *f;
    int x = col-1;
    int y = row-1;
    while(x >= 0 && y >= 0) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        x--;
        y--;
    }
    x = col+1;
    y = row+1;
    while(x < 8 && y < 8) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        x++;
        y++;
    }
    x = col+1;
    y = row-1;
    while(y >= 0 && x < 8) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        y--;
        x++;
    }
    x = col-1;
    y = row+1;
    while(y < 8 && x >= 0) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        y++;
        x--;
    }
}

// Rook
void Rook::addSelects(std::vector<Selection *> &selects) {
    selects.push_back(new Selection(this->col, this->row, selection_t::SELECTED));
    Figure *f;
    int x = col-1;
    while(x >= 0) {
        if((f = board->get_figure_at(x, row)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, row, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, row, selection_t::MOVABLE));
        x--;
    }
    x = col+1;
    while(x < 8) {
        if((f = board->get_figure_at(x, row)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, row, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, row, selection_t::MOVABLE));
        x++;
    }
    int y = row-1;
    while(y >= 0) {
        if((f = board->get_figure_at(col, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(col, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(col, y, selection_t::MOVABLE));
        y--;
    }
    y = row+1;
    while(y < 8) {
        if((f = board->get_figure_at(col, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(col, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(col, y, selection_t::MOVABLE));
        y++;
    }
}

// Queen
void Queen::addSelects(std::vector<Selection *> &selects) {
    selects.push_back(new Selection(this->col, this->row, selection_t::SELECTED));
    Figure *f;
    int x = col-1;
    int y = row-1;
    while(x >= 0 && y >= 0) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        x--;
        y--;
    }
    x = col+1;
    y = row+1;
    while(x < 8 && y < 8) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        x++;
        y++;
    }
    x = col+1;
    y = row-1;
    while(y >= 0 && x < 8) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        y--;
        x++;
    }
    x = col-1;
    y = row+1;
    while(y < 8 && x >= 0) {
        if((f = board->get_figure_at(x, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, y, selection_t::MOVABLE));
        y++;
        x--;
    }
    x = col-1;
    while(x >= 0) {
        if((f = board->get_figure_at(x, row)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, row, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, row, selection_t::MOVABLE));
        x--;
    }
    x = col+1;
    while(x < 8) {
        if((f = board->get_figure_at(x, row)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(x, row, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(x, row, selection_t::MOVABLE));
        x++;
    }
    y = row-1;
    while(y >= 0) {
        if((f = board->get_figure_at(col, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(col, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(col, y, selection_t::MOVABLE));
        y--;
    }
    y = row+1;
    while(y < 8) {
        if((f = board->get_figure_at(col, y)) != nullptr) {
            if(f->isWhite() != white) {
                selects.push_back(new Selection(col, y, selection_t::THREATENED));
                break;
            }
            break;
        }
        selects.push_back(new Selection(col, y, selection_t::MOVABLE));
        y++;
    }
}

// King
void King::addKingSelects(std::vector<Selection *> &selects) {
    for(int setCol: {col-1, col, col+1}) {
        for(int setRow: {row-1, row, row+1}) {
            selects.push_back(new Selection(setCol, setRow, selection_t::MOVABLE));
        }
    }
}

void King::addSelects(std::vector<Selection *> &selects) {
    selects.push_back(new Selection(this->col, this->row, selection_t::SELECTED));
    int prevCol = col;
    int prevRow = row;
    selects.push_back(new Selection(col, row, selection_t::SELECTED));
    for(int setCol: {col-1, col, col+1}) {
        for(int setRow: {row-1, row, row+1}) {
            King *k = board->getBlackKing();
            if(!white) {
                k = board->getWhiteKing();
            }
            std::vector<Selection *> kingSels;
            k->addKingSelects(kingSels);
            bool threatened = false;
            for(Selection *s: kingSels) {
                if(s->getCol() == setCol && s->getRow() == setRow) {
                    threatened = true;
                }
            }
            if(threatened)
                continue;

            if(setCol >= 0 && setRow <= 7 && setCol <= 7 && setRow >= 0) {
                if(board->get_figure_at(setCol, setRow) == nullptr) {
                    setColRow(setCol, setRow);
                    board->set_figure_at(setCol, setRow, this);
                    if(!board->is_check(setCol, setRow, white)) {
                        selects.push_back(new Selection(setCol, setRow, selection_t::MOVABLE));
                    }
                    setColRow(prevCol, prevRow);
                    board->set_figure_at(setCol, setRow, nullptr);
                }
                else if(board->get_figure_at(setCol, setRow) != nullptr && board->get_figure_at(setCol, setRow)->isWhite() != white) {
                    Figure *prevFig = board->get_figure_at(setCol, setRow);
                    setColRow(setCol, setRow);
                    board->set_figure_at(setCol, setRow, this);

                    if(!board->is_check(setCol, setRow, white)) {
                        selects.push_back(new Selection(setCol, setRow, selection_t::THREATENED));
                    }
                    setColRow(prevCol, prevRow);
                    board->set_figure_at(setCol, setRow, prevFig);
                }
            }
        }
    }
}