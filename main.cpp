#include <iostream>
#include <algorithm>

#include "game.h"

int main() {
    using namespace std;

    string path = "";
    string savep = "saved_match.txt";
    Game *g;
    try {
        g = new Game(path);
    }catch(ChessException *e){
        cerr << "ERROR: " << e->get_message() << endl;
    }

    int col;
    int row;
    string s;
    do {
        g->print();

        cin >> s;

        if(s[0] == 'n')
            g->next_move();
        else if(s[0] == 'p')
            g->prev_move();
        else if(s[0] == 'r')
            g->reset();
        else if(std::isdigit(s[0])) {
            cout << "COL: " << s << endl;
            col = atoi(s.c_str());
            cout << "ROW: ";
            cin >> row;
            g->selected(col, row);
        }
        else if(s[0] == 's')
            g->save(savep);
        s = "";
    }while (col != -1);
    return 0;
}