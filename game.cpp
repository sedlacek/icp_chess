/**
 * Game module
 * Works as adapter making new API for easy communication between the game (board and notation) and the GUI
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#include "game.h"

Game::Game(std::string path) {
    this->board = new Board();
    this->notation = new Notation(path, this->board);
    this->notation->parse();
    this->notation->check_moves();
    this->board->set_notation(this->notation);
    reset();
}

void Game::selected(int col, int row) {
    board->selected(col, row);
}

void Game::reset() {
    board->reset();
    notation->reset();
}

void Game::next_move() {
    notation->next_move();
}

void Game::prev_move() {
    notation->prev_move();
}

void Game::set_move(int number) {
    notation->set_move(number);
}

void Game::save(std::string path) {
    notation->save(path);
}

std::vector<Figure *> Game::get_figures(){
    return this->board->get_figures();
}

std::vector<Selection *> Game::get_selections(){
    return this->board->get_selections();
}

int Game::get_current_move() {
    return notation->get_current_move();
}

std::vector<NotationRow> Game::get_notation() {
    return notation->format();
}