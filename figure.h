/**
 * Figure module
 * Holds classes for all figures on the board
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#ifndef CHESS_FIGURE_H
#define CHESS_FIGURE_H

#include "board.h"
#include "selection.h"
#include <iostream>
#include <vector>

class Board;

/**
 * Enum type telling which figure we're working with so that there is no need for dynamic checking
 */
enum class figure_t{
    PAWN,
    KNIGHT,
    BISHOP,
    ROOK,
    QUEEN,
    KING
};

/**
 * Figure class - parent class to all chess pieces
 */
class Figure {
protected:
    bool white;
    int col;
    int row;
    std::string identificator;
    Board *board;
    figure_t type;
public:
    /**
     * Constructor
     * @param col Column position of the piece
     * @param row Row position of the piece
     * @param white If the piece is white
     * @param board Board to which is this piece bound
     * @param type Type of the piece
     * @param identificator It's notation identificator
     */
    Figure(int col, int row, bool white, Board *board, figure_t type, std::string &&identificator);

    /**
     * Adds all possible fields to which the piece could move/attack...
     * @param selects vector to which will be the selections appended
     */
    virtual void addSelects(std::vector<Selection *> &selects) = 0; // Abstract method

    // Getters and setters
    /**
     * @return true if the piece is white
     */
    bool isWhite();

    /**
     * @return pieces column position
     */
    int getCol();

    /**
     * @return pieces row position
     */
    int getRow();

    /**
     * @return pieces identificator
     */
    std::string getIdentificator();

    /**
     * @return pieces type
     */
    figure_t getType();

    /**
     * Sets pieces column and row
     * @param col column
     * @param row row
     */
    void setColRow(int col, int row);
};

/**
 * Pawn class
 */
class Pawn: public Figure {
public:
    /**
     * Constructor
     * @param col Column position of the piece
     * @param row Row position of the piece
     * @param white If the piece is white
     * @param board Board to which is this piece bound
     */
    Pawn(int col, int row, bool white, Board *board): Figure(col, row, white, board, figure_t::PAWN, ""){}
    virtual void addSelects(std::vector<Selection *> &selects);
};

/**
 * Knight class
 */
class Knight: public Figure {
public:
    /**
     * Constructor
     * @param col Column position of the piece
     * @param row Row position of the piece
     * @param white If the piece is white
     * @param board Board to which is this piece bound
     */
    Knight(int col, int row, bool white, Board *board): Figure(col, row, white, board, figure_t::KNIGHT, "J"){}
    virtual void addSelects(std::vector<Selection *> &selects);
};

/**
 * Bishop class
 */
class Bishop: public Figure {
public:
    /**
     * Constructor
     * @param col Column position of the piece
     * @param row Row position of the piece
     * @param white If the piece is white
     * @param board Board to which is this piece bound
     */
    Bishop(int col, int row, bool white, Board *board): Figure(col, row, white, board, figure_t::BISHOP, "S"){}
    virtual void addSelects(std::vector<Selection *> &selects);
};

/**
 * Rook class
 */
class Rook: public Figure {
public:
    /**
     * Constructor
     * @param col Column position of the piece
     * @param row Row position of the piece
     * @param white If the piece is white
     * @param board Board to which is this piece bound
     */
    Rook(int col, int row, bool white, Board *board): Figure(col, row, white, board, figure_t::ROOK, "V"){}
    virtual void addSelects(std::vector<Selection *> &selects);
};

/**
 * Queen class
 */
class Queen: public Figure {
public:
    /**
     * Constructor
     * @param col Column position of the piece
     * @param row Row position of the piece
     * @param white If the piece is white
     * @param board Board to which is this piece bound
     */
    Queen(int col, int row, bool white, Board *board): Figure(col, row, white, board, figure_t::QUEEN, "D"){}
    virtual void addSelects(std::vector<Selection *> &selects);
};

/**
 * King class
 */
class King: public Figure {
private:
    /**
     * Adds fields to which the king could move (3*3 square)
     * @param selects vector to which will be these selections appended
     */
    void addKingSelects(std::vector<Selection *> &selects);
public:
    /**
     * Constructor
     * @param col Column position of the piece
     * @param row Row position of the piece
     * @param white If the piece is white
     * @param board Board to which is this piece bound
     */
    King(int col, int row, bool white, Board *board): Figure(col, row, white, board, figure_t::KING, "K"){}
    virtual void addSelects(std::vector<Selection *> &selects);
};

#endif //CHESS_FIGURE_H
