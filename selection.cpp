/**
 * Selection module
 * Module for working with highlights on the board
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#include "selection.h"

Selection::Selection(int col, int row, selection_t type) {
    this->col = col;
    this->row = row;
    this->type = type;
}

int Selection::getCol() {
    return this->col;
}

int Selection::getRow() {
    return this->row;
}

selection_t Selection::getType() {
    return this->type;
}

std::string Selection::getBGColor() {
    if(this->type == selection_t::THREATENED){
        return "\033[1;45m";
    }
    return "\033[1;42m";
}