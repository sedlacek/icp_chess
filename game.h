/**
 * Game module
 * Works as adapter making new API for easy communication between the game (board and notation) and the GUI
 * @authors Marek Sedláček (xsedla1b)
 *          Ján Pavlus (xpavlu10)
 */

#ifndef CHESS_GAME_H
#define CHESS_GAME_H

#include "board.h"
#include "notation.h"

class Game {
private:
    Board *board;
    Notation *notation;
public:
    /**
     * Constructor
     * Reads input file notations, parses it and creates board with the game
     * @param path Path to the file with the game notation
     */
    Game(std::string path);

    /**
     * Saves current game notation into passed in file
     * @param path Path to the file
     */
    void save(std::string path);

    /**
     * Selects field/piece (making move or showing selections)
     * @note To move a piece, first call this method on filed with the piece you want to move,
     *       then call this method again with the field you want to move the piece to
     * @param col Column of the field/piece
     * @param row Row of the field/piece
     */
    void selected(int col, int row);

    /**
     * Resets the game to starting position
     */
    void reset();

    /**
     * Makes next move if possible
     */
    void next_move();

    /**
     * Undo a move if possible
     */
    void prev_move();

    /**
     * Set the game to position (move) passed in
     * Numbering starts from 0 (1st move is number 0)
     * @param number position to move to
     */
    void set_move(int number);


    /**
     * Returns all figures that are on the board
     * @return Vector of figure pointers
     */
    std::vector<Figure *> get_figures();

    /**
     * Returns all selections that are now present
     * @return Vector of selection pointers
     */
    std::vector<Selection *> get_selections();

    /**
     * @return Formatted notation
     */
    std::vector<NotationRow> get_notation();

    /**
     * Returns current active move (0 = no move played yet, 2 = blacks 1st move)
     * @return current move
     */
    int get_current_move();
#ifdef DEBUG
    /**
     * Renders board in ASCII (in colors on linux)
     */
    void print(){board->print(std::cout);}
#endif
};

#endif //CHESS_GAME_H
